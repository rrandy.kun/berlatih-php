<?php
function tentukan_nilai($number)
{
    if ($number >= 85) {
        $hasil = "Sangat Baik<br>";
    } elseif ($number >= 70) {
        $hasil = "Baik<br>";
    } elseif ($number >= 60) {
        $hasil = "Cukup<br>";
    } else {
        $hasil = "Kurang<br>";
    }

    return $hasil;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
