<?php
function ubah_huruf($string)
{
    $huruf = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $temp = substr($string, $i, 1);
        $huruf .= ++$temp;
    }
    $huruf .= "<br>";
    return $huruf;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu